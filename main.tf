/*

*/

terraform {
  backend "s3" {
    bucket = "plochk-terraform-state"
    key    = "terraform.tfstate"
    region = "eu-central-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
    region = local.region
}

locals {
    // Frankfurt
    region = "eu-central-1"
    debian11-ami = "ami-0a5b5c0ea66ec560d"
}

resource "aws_vpc" "nextcloud" {
  cidr_block = "10.13.0.0/16"
  tags = {
    Name = "Nextcloud VPC"
  }
}

resource "aws_internet_gateway" "nextcloud" {
  vpc_id = aws_vpc.nextcloud.id

  tags = {
    Name = "Nextcloud Internet Gateway"
  }
}

resource "aws_subnet" "nextcloud-public" {
  vpc_id                          = aws_vpc.nextcloud.id
  availability_zone               = "${local.region}a"
  cidr_block                      = "10.13.10.0/24"
  assign_ipv6_address_on_creation = false
  // Public IP Address
  map_public_ip_on_launch         = true

  tags = {
    Name = "Nextcloud Public"
  }
}

resource "aws_subnet" "nextcloud-backend-a" {
  vpc_id                          = aws_vpc.nextcloud.id
  availability_zone               = "${local.region}a"
  cidr_block                      = "10.13.20.0/24"
  assign_ipv6_address_on_creation = false
  # Public IP Address
  map_public_ip_on_launch         = false

  tags = {
    Name = "Nextcloud Backend"
  }
}

resource "aws_subnet" "nextcloud-backend-b" {
  vpc_id                          = aws_vpc.nextcloud.id
  availability_zone               = "${local.region}b"
  cidr_block                      = "10.13.30.0/24"
  assign_ipv6_address_on_creation = false
  # Public IP Address
  map_public_ip_on_launch         = false

  tags = {
    Name = "Nextcloud Backend"
  }
}

resource "aws_route_table_association" "nextcloud-public" {
  subnet_id      = aws_subnet.nextcloud-public.id
  route_table_id = aws_route_table.nextcloud-public.id
}

resource "aws_route_table_association" "nextcloud-backend-a" {
  subnet_id      = aws_subnet.nextcloud-backend-a.id
  route_table_id = aws_route_table.nextcloud-backend.id
}

resource "aws_route_table_association" "nextcloud-backend-b" {
  subnet_id      = aws_subnet.nextcloud-backend-b.id
  route_table_id = aws_route_table.nextcloud-backend.id
}

resource "aws_route_table" "nextcloud-public" {
  vpc_id = aws_vpc.nextcloud.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.nextcloud.id
  }

  tags = {
    Name = "Nextcloud Route Table"
  }
}

resource "aws_route_table" "nextcloud-backend" {
  vpc_id = aws_vpc.nextcloud.id

  tags = {
    Name = "Nextcloud Route Table"
  }
}

resource "aws_security_group" "nextcloud-public-web" {
  name        = "nextcloud-public-web"
  description = "Allow access from 0.0.0.0/0 80/443 and allow machine to update"
  vpc_id      = aws_vpc.nextcloud.id

  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http_s"
  }
}

resource "aws_security_group" "nextcloud-backend-rds" {
  name        = "nextcloud-backend-rds"
  description = "Allow MariaDB access from nextcloud application"
  vpc_id      = aws_vpc.nextcloud.id

  ingress {
    description      = "PostgreSQL TCP"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    cidr_blocks      = [aws_subnet.nextcloud-public.cidr_block]
  }

  ingress {
    description      = "PostgreSQL UDP"
    from_port        = 3306
    to_port          = 3306
    protocol         = "udp"
    cidr_blocks      = [aws_subnet.nextcloud-public.cidr_block]
  }

  tags = {
    Name = "allow-mariadb-from-public"
  }
}

resource "aws_key_pair" "freyr" {
    key_name = "freyr"
    public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE9PnXeSCd9NLLagahH4wm5vVlWBrKhR85iaOoVskLF5 freyr aws access key"
}

resource "aws_key_pair" "njord" {
    key_name = "njord"
    public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIEaudBYKPRCexM0aMdmXUg76SbDQqgAxcJBCOdfs4xg aws nextcloud njord access key"
}

resource "aws_instance" "nextcloud-app" {
    availability_zone           = "${local.region}a"
    associate_public_ip_address = true
    subnet_id                   = "${aws_subnet.nextcloud-public.id}"

    ami = "${local.debian11-ami}"
    instance_type = "t2.micro"

    key_name               = aws_key_pair.njord.id
    vpc_security_group_ids = ["${aws_security_group.nextcloud-public-web.id}"]

    tags = {
        Name = "Nextcloud Application"
    }
}

resource "aws_db_subnet_group" "nextcloud-mariadb" {
  name       = "nextcloud-mariadb"
  subnet_ids = [ aws_subnet.nextcloud-backend-a.id,
                 aws_subnet.nextcloud-backend-b.id ]

  tags = {
    Name = "Nextcloud DB subnet group"
  }
}

resource "aws_db_instance" "nextcloud-mariadb" {
    availability_zone      = "${local.region}a"
    multi_az               = false
    publicly_accessible    = false
    vpc_security_group_ids = [aws_security_group.nextcloud-backend-rds.id]
    db_subnet_group_name   = aws_db_subnet_group.nextcloud-mariadb.id

    instance_class         = "db.t3.micro"
    storage_type           = "gp2"
    allocated_storage      = 20
    max_allocated_storage  = 0
    engine                 = "mariadb"
    engine_version         = "10.6.7"
    port                   = 3306
    // db_name = "nextcloud-db"
    name                   = "nextcloud"

    username               = "nextcloud"
    password               = "nextcloud"

    backup_retention_period = 0
    skip_final_snapshot     = true
}
